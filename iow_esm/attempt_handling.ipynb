{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Attempt handling\n",
    "\n",
    "## 1. Set up the IOW ESM framework\n",
    "\n",
    "Follow the steps described in the Readme you can find at the project's [main site](https://git.io-warnemuende.de/iow_esm/main).\n",
    "\n",
    "After you have set up everything go to the target machine. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Create your own attempt handler\n",
    "\n",
    "Once you are on the target you have to create a python module, let's call it `my_attempt_handling.py`.\n",
    "You can create it anywhere you like but let's assume for this example that you create it in the path to the root directory of the IOW ESM, say `\"/path/to/IOW_ESM\"`.\n",
    "\n",
    "This module has to contain a class with some mandatory members and methods as illustrated in the following example:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "``` python\n",
    "# imports for this example, you can import arbitrary modules here\n",
    "import os\n",
    "import glob\n",
    "\n",
    "# class name is arbitrary\n",
    "class MyAttemptHandler():\n",
    "    \n",
    "        # constructor can have arbitrary arguments\n",
    "        def __init__(self, root = \".\"):\n",
    "                # !!! mandatory member attempts must be a list of strings\n",
    "                self.attempts = [\"1\", \"2\"]\n",
    "                # !!! mandatory member last_attempt_file must be a name of a file (can be a path, absolute or relative to \"/path/to/IOW_ESM/scripts/run\") that can be created and read\n",
    "                self.last_attempt_file = \"my_last_attempt.txt\"\n",
    "\n",
    "                # optional arguments and members\n",
    "                # it makes sense to memorize the root directory\n",
    "                self.root = root\n",
    "\n",
    "        # !!! mandatory method for preparing an attempt, signature is obligatory\n",
    "        def prepare_attempt(self, attempt):\n",
    "\n",
    "                if attempt == self.attempts[0]:\n",
    "                        # do nothing for the first attempt\n",
    "                        return\n",
    "\n",
    "                if attempt == self.attempts[1]:\n",
    "                        # do something with input files for second attempt\n",
    "                        os.system(\"touch \" + self.root + \"/attempt2.txt\")\n",
    "                        return\n",
    "\n",
    "                return\n",
    "\n",
    "        # !!! mandatory method for evaluating an attempt, signature is obligatory, must return True or False\n",
    "        def evaluate_attempt(self, attempt):\n",
    "\n",
    "                # in this example let's provoke that the first attempt fails\n",
    "                if attempt == self.attempts[0]:\n",
    "                        return False\n",
    "\n",
    "                # let the second attempt succeed if file from preparation has been created\n",
    "                if attempt == self.attempts[1]:\n",
    "                        if glob.glob(self.root + \"/attempt2.txt\") != []:\n",
    "                                return True\n",
    "\n",
    "                return True\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Read the comments in this code and understand what is happening.\n",
    "This dummy example handles two attempts called `\"1\"` and `\"2\"`, see member `attempts`.\n",
    "While creation, the object gets an additional member for memorizing the root path of the project. \n",
    "However you can pass arbitrary arguments to the constructor.\n",
    "The example class does nothing to prepare the first attempt but creates an empty file for the second attempt, see the `prepare_attempt` method.\n",
    "It is further constructed such that the first attempt will always fail and the second succeeds only if the empty file has been created, `evaluate_attempt` method.\n",
    "\n",
    "Of course for your application you have to implement your own functionality according to your needs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Register your attempt handler\n",
    "\n",
    "In order to register your attempt handler you have to pass an instance via the `global_settings.py` in the `input` folder within your `\"/path/to/IOW_ESM\"`.\n",
    "For the example given above you would have to add the following lines to the `global_settings.py` file."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "``` python\n",
    "#################################################\n",
    "# STEP 3a: Attempt handling                     #\n",
    "#################################################\n",
    "\n",
    "# import own module \n",
    "import sys\n",
    "sys.path.append(\"/path/to/IOW_ESM\") # you can put absolute paths here, if not this path is interpreted relative to scripts/run \n",
    "import my_attempt_handling\n",
    "\n",
    "# create instance of the attempt handler\n",
    "attempt_handler = my_attempt_handling.MyAttemptHandler(\"/path/to/IOW_ESM\")                      # if a run fails, you can have new attempts with modified settings\n",
    "\n",
    "```\n",
    "\n",
    "Again the name `my_attempt_handling`, `MyAttemptHandler` as well as the path `\"/path/to/IOW_ESM\"` might be different for you."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. How the IOW ESM calls the attempt handler \n",
    "\n",
    "The IOW ESM will call the attempt handler such that it iterates over the entries in `attempt_handler.attempts` until the model does not crash and `evaluate_attempt` returns `True`, or the last attempt is exhausted.\n",
    "\n",
    "The corresponding code from `scripts/run/run.py` looks like:\n",
    "\n",
    "``` python\n",
    "for run in range(runs_per_job):\n",
    "        \n",
    "    # get next attempt from the file specified in member last_attempt_file (done by an attempt_iterator object)\n",
    "    attempt = attempt_iterator.get_next_attempt()\n",
    "\n",
    "    # prepare the attempt\n",
    "    attempt_handler.prepare_attempt(attempt)\n",
    "\n",
    "    # perform attempt\n",
    "    # ...\n",
    "    # set run_failed = True if the model crashed\n",
    "\n",
    "    # if the run did not crash, still check for evaluation\n",
    "    if not run_failed:\n",
    "        run_failed = not attempt_handler.evaluate_attempt(attempt)\n",
    "    \n",
    "    # something went wrong: either model has crashed or the attempt has not passed the criterion \n",
    "    if run_failed:\n",
    "        # if this was the final attempt, we stop here\n",
    "        if attempt == attempt_handler.attempts[-1]:\n",
    "            print('IOW_ESM job finally failed integration from '+str(start_date)+' to '+str(end_date))\n",
    "            sys.exit()\n",
    "\n",
    "        # if it was not the final attempt, memorize this attempt (done by an attempt_iterator object)\n",
    "        attempt_iterator.store_last_attempt(attempt)\n",
    "        \n",
    "        # go on with next attempt in a new job\n",
    "        print('  attempt '+str(attempt)+' failed. Go on with next attempt.', flush=True)\n",
    "        os.system(\"cd \" + IOW_ESM_ROOT + \"/scripts/run; \" + resubmit_command)\n",
    "        sys.exit()\n",
    "\n",
    "```\n",
    "\n",
    "**Important**\n",
    "* the preparation takes place _before_ the input files are copied to the local work directories\n",
    "* the evaluation takes place _after_ the output is copied to the global `work` directory but _before_ it is stored in the `output` directory\n",
    "\n",
    "In other words you best prepare your attempt in the `input` directory and evaluate it in the global `work` directory.\n",
    "\n",
    "Note that preparation and evaluation are intended to be rather small and fast tasks since these steps consume walltime of your job. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The default case\n",
    "\n",
    "The default for the variable `attempt_handler` would be `None` or it can be left out.\n",
    "In both cases there will be a single attempt for each run without preparation and always positive evaluation (however a run can of cource  still fail if the model crashes)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "(1) Python 3",
   "language": "python",
   "name": "iow_python"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
