import os
import pathlib
root = "."

# toc = {}

toc = []

for path, subdirs, files in os.walk(root):
    for name in files:
#        print(path + ' - ' + name)
#        if "notebook_collection" in path:
        if not ".ipynb_checkpoints" in path:
          if not "_build" in path:
            if ".ipynb" in name:
              currentFile = str(pathlib.PurePath(path, name))
              toc.append(currentFile)
                      #  print(currentFile)

f = open('_toc.yml', 'w')
f.write('format: jb-book\n')
f.write("root: intro.md\n")
f.write("parts:\n")

Chapter = ""

for element in toc:
    filename = os.path.basename(element)
    chapter = element.replace("notebook_collection/","").split("/")[0]
    if chapter != Chapter:
        Chapter = chapter
        f.write(f"- caption: {Chapter}\n")
        f.write("  chapters:\n")
    f.write(f"  - file: {element}\n")
f.close()  #
